(function($){

  const menu_2019 = {};
  menu_2019.item_submenu = $("ul.dropdown-menu .dropdown-submenu [data-toggle='dropdown'] ");
  menu_2019.item_menu =  $("ul.dropdown-menu [data-toggle='dropdown']");
  menu_2019._width = $(window).width();
  
    menu_2019.item_menu.on("click", function(event) {
      event.preventDefault();
      event.stopPropagation();
     
      var _this =$(this);
      _this.siblings().toggleClass("show");
      if (!_this.next().hasClass('show')) {
        _this.parents('.dropdown-menu').first().find('.show').removeClass("show");
      }
      _this.parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
      
        $('.dropdown-submenu .show').removeClass("show");
        $('.dropdown-submenu .active').removeClass('active');
      });
    });

    if(menu_2019._width > 1024) {
      $('ul.nav li.dropdown').hover(function() {
        const _this =$(this);
        _this.addClass('open').find('>.dropdown-menu').addClass('show');
        _this.find('>.dropdown-toggle').attr('aria-expanded',true);
      }, function() {
        const _this =$(this);
        _this.removeClass('open').find('>.dropdown-menu').removeClass('show');
        _this.find('>.dropdown-toggle').attr('aria-expanded',false);
      });
      
      $('ul.dropdown-menu li.dropdown-submenu').hover(function() {

        const _this =$(this);
        _this.addClass('open').find('>.dropdown-menu').addClass('show');

      }, function() {
        const _this =$(this);
        _this.removeClass('open').find('>.dropdown-menu').removeClass('show');
        
      });
    }
   
    menu_2019.item_submenu.on('click', function(event){
      event.preventDefault();
      event.stopPropagation();

      var _this =$(this);
      if(_this.hasClass('active')){
        _this.removeClass('active').siblings('.dropdown-menu').removeClass('show');
        return;
      }
      _this.closest('.dropdown-menu').find('.dropdown-submenu .dropdown-menu').removeClass('show');
      _this.closest('.dropdown-menu').find('.dropdown-submenu .dropdown-toggle').removeClass('active');
    
      _this.siblings().toggleClass("show");
      _this.toggleClass('active');
    });
  
  
  
})(jQuery)
