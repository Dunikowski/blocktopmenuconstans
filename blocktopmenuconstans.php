<?php 

if (!defined('_PS_VERSION_'))
    exit;
    
class blocktopmenuconstans extends Module{

    protected $meble = [
        '472'=>[
                '473' =>['500'=>'','478'=>'','490'=>'','491'=>'','488'=>'','496'=>''],
                '503'=>['505'=>'','506'=>'','507'=>'','509'=>'','511'=>'','513'=>'','514'=>''],
                '516'=>['518'=>'','527'=>'','528'=>'','529'=>'','530'=>'','531'=>'','532'=>'','534'=>''],
                '537'=>['538'=>'','539'=>'','540'=>''],
                '541'=>['543'=>'','544'=>'','545'=>''],
                '50'=>['73'=>'','72'=>'','75'=>'','71'=>''],
                '551'=>'',
                '552'=>'',
                '553'=>'',
                '33'=>['57'=>'','56'=>'','54'=>'','562'=>'','55'=>'','58'=>''],
                '65'=>'',
                '339'=>''
                ],
        '26' => ['27'=>'','40'=>'','39'=>'','584'=>'','447'=>'','446'=>'','445'=>'','448'=>'','459'=>'','458'=>''],
        '2'=>[
                '24' =>['25'=>'','35'=>'','339'=>'','28'=>'','428'=>'','36'=>'','38'=>'','83'=>'','37'=>'','471'=>'','429'=>''],
                '32'=>['34'=>'','43'=>'','587'=>'','44'=>'','47'=>'','46'=>'','45'=>'','443'=>'','468'=>''],
                '423'=>['435'=>'','437'=>'','438'=>'','439'=>'','456'=>'','461'=>'','465'=>'','469'=>''],
                '41'=>['59'=>'','60'=>'','63'=>'','61'=>'','62'=>''],
                '42'=>['64'=>'','70'=>'','65'=>'','66'=>'','68'=>'','434'=>'','69'=>'','67'=>''],
                '427'=>'',
                '215'=>['218'=>'','219'=>'','220'=>'','216'=>'','413'=>'','604'=>''],
                '51'=>['76'=>'','77'=>'','78'=>'','79'=>'','467'=>''],
                '424'=>['453'=>'','455'=>'','463'=>'','466'=>'','93'=>'','288'=>''],
                '425'=>'',
                '426'=>'',
            ],
    ];
    public function __construct(){
        $this->name = 'blocktopmenuconstans';
        $this ->tab = 'front_office_features';
        $this->version = '0.1';
        $this->author = 'AdVist';
        $this->displayName = 'Menu width constant categories';
        $this->bootstrap = true;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => '1.6.99.99');
        parent::__construct();
        
        $this->displayName = $this->l('Menu with permanent categories');
        $this->description = $this->l('Adds menu  to the top with specific categories.');
       
    }
    
    public function install()
    {
        parent::install();
        $this->registerHook('header');
        $this->registerHook('displayTop');
        return true;
    }
     public function hookHeader()
     {
       
         $this->context->controller->addCSS(($this->_path).'css/blocktopmenuconstans.css');
         $this->context->controller->addJS(($this->_path).'js/blocktopmenuconstans.js');
     }



     public function getContent()
     {
         $output = '';
         if (Tools::isSubmit('submitBlockTopMenyConstans'))
         {
            Configuration::updateValue('BLOCKTOPMENUCONSTANS_BESTSELLERS_CATEGORY', Tools::getValue('bestsellers_category'));
            Configuration::updateValue('BLOCKTOPMENUCONSTANS_BESTSELLERS_NUMBERS', Tools::getValue('bestsellers_numbers'));
              
         }
         return $output.$this->renderForm();
     }
     public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(

					array(
						'type' => 'text',
						'label' => $this->l('Enter id:'),
						'name' => 'bestsellers_category',
						'desc' => $this->l('Select the id of the bestsellers category.'),
                    ),
                    array(
						'type' => 'text',
						'label' => $this->l('Number of products:'),
						'name' => 'bestsellers_numbers',
						'desc' => $this->l('Insert the number of products displayed.'),
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitBlockTopMenyConstans';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' =>  $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);

		return $helper->generateForm(array($fields_form));
	}
        public function getConfigFieldsValues()
        {
            return array(
                'bestsellers_category' =>Tools::getValue('bestsellers_category', Configuration::get('BLOCKTOPMENUCONSTANS_BESTSELLERS_CATEGORY')),
                'bestsellers_numbers' => Tools::getValue('bestsellers_numbers', Configuration::get('BLOCKTOPMENUCONSTANS_BESTSELLERS_NUMBERS')),
             
            );
        }
    protected function generateMenu($array,$level = 0){

        $html = '';
        foreach($array as $key => $value){

            $isArray = is_array($value);
        
            $cat = new Category($key);
            $link = Tools::HtmlEntitiesUTF8($cat->getLink());
            
            if($isArray) {
                if($level > 0){
                    $html .= '<li class="nav-item dropdown-submenu">';
                }else{
                    $html .= '<li class="nav-item dropdown">';
                }
            }
            else{
                $html .= '<li class="nav-item">';
            }

            if($isArray){
                if(strstr($link,"2-glowna")) {
                    $html .= '<a class="nav-link"  href="#" >Pomieszczenie</a><a href="#"  class="dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>';
                } else {
                    $html .= '<a class="nav-link"  href="'.$link.'" >'.$cat->name[1].'</a><a href="#" class="dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>';   
                }
                $html .= '<ul class="dropdown-menu"  aria-labelledby="navbarDropdown">'.$this->generateMenu($value, $level + 1).'</ul>';

            } else{

                $html .= '<a class="nav-link" href="'.$link.'">'.$cat->name[1].'</a>';
            }
            $html .= '</li>';
        }

        return $html;
    }
    public function hookDisplayTop($params)
    {
      $bestsellers_count = 1; 
      $bestsellers_category = '';
     

      $this->smarty->assign('MENU', $this->generateMenu($this->meble));

      
      $bestsellers_category = Configuration::get('BLOCKTOPMENUCONSTANS_BESTSELLERS_CATEGORY');

      if($bestsellers_category != ''){

        if(Configuration::get('BLOCKTOPMENUCONSTANS_BESTSELLERS_NUMBERS')){
            $bestsellers_count = (int)Configuration::get('BLOCKTOPMENUCONSTANS_BESTSELLERS_NUMBERS');
          }

        $acc_category = new Category($bestsellers_category);
        $acc_products =  $acc_category->getProducts($this->context->language->id, 1, $bestsellers_count);
        $this->smarty->assign('_bestsellers',  $acc_products);
        $this->smarty->assign('_bestsellers_link',   Tools::HtmlEntitiesUTF8($acc_category->getLink()));
      }else{
        $this->smarty->assign('_bestsellers',  null);
      }
       
        $html = $this->display(__FILE__, 'blocktopmenuconstans.tpl');
       
        return $html;
    }
}