{if $MENU != ''}
	<!-- Menu -->
	<nav class="navbar navbar-expand-lg bockconstanscaterories">
	
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="nav navbar-nav">
				{$MENU}
				{if $_bestsellers}
				<li class="nav-item dropdown">
					<a class="nav-link"  href="{$_bestsellers_link}" >{l s='Bestsellers' mod='blocktopmenuconstans'}</a><a href="#" class="dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
						<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
						{foreach from=$_bestsellers item=item}
							<li class="nav-item"><a class="nav-link" href="{$item['link']}">{$item['name']}</a></li>
						{/foreach}
						</ul>
				</li>
				{/if}
				<li class="nav-item discount">
					<a class="nav-link" href="{$link->getPageLink('discount')}">{l s='discount' mod='blocktopmenuconstans'}</a>
				</li>
				<li class="nav-item">
					<a class="nav-link prices-drop" href="{$link->getPageLink('prices-drop')}">{l s='prices-drop' mod='blocktopmenuconstans'}</a>
				</li>
			</ul>
		</div>
	</nav>

	<!--/ Menu -->
{/if}